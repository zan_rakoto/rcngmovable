module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> (<%= pkg.homepage %>) */\n'
            },
            rcNgMovable: {
                files: {
                    './js/rcNgMovable.min.js': ['./js/rcNgMovable.js','./js/rcTransformService.js']
                }
            }
        },
        jshint: {
            options: {
                ignores: ['./js/*.min.js']
            },
            files: ['./js/*.js']
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['js/rcNgMovable.min.js', 'js/rcTransformService.min.js'],
                dest: 'dist/rcNgMovable.min.js'
            },
            dev: {
                src: ['js/rcNgMovable.js', 'js/rcTransformService.js'],
                dest: 'dist/rcNgMovable.js'
            }
        },
        clean: {
            build: {
                src: ["dist"]
            }
        },
        connect: {
            server: {
                options: {
                    port: 9003,
                    open: {
                        target: 'http://localhost:9003/example/index.html'
                    }
                }
            }
        },
        watch: {
            js: {
                files: ['js/*.js','!js/*.min.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            html: {
                files: ['example/*.html'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('cleanbuild', ['clean']);
    grunt.registerTask('default', ['jshint','connect','watch']);
    grunt.registerTask('build', ['jshint','uglify']);
    grunt.registerTask('js', ['jshint']);
};