(function (window, angular) {
    'use strict';

    Math.degToRad = function (deg) {
        return deg * Math.PI / 180;
    };

    Math.radToDeg = function (rad) {
        return rad * 180 / Math.PI;
    };

    var module = angular.module('rcNgMovable', []);
    module
        .constant('MovableHammer', Hammer)
        .constant('$M', $M)
        .factory('rcTransformService', function () {
            return {

                setElTransformStyle: function (el, transform) {
                    var elem;
                    if (typeof el === "string")
                        elem = document.getElementById(el);
                    else
                        elem = el;

                    elem.style.transform = transform;
                    elem.style.oTransform = transform;
                    elem.style.msTransform = transform;
                    elem.style.mozTransform = transform;
                    elem.style.webkitTransform = transform;

                    /*var transition = 'all 80ms linear';
                    elem.style.webkitTransition = transition;
                    elem.style.mozTransition = transition;
                    elem.style.oTransition = transition;
                    elem.style.transition = transition;*/
                },

                setElTransformOriginStyle: function (el, transformOrigin) {
                    var elem;
                    if (typeof elem === "string")
                        elem = document.getElementById(el);
                    else
                        elem = el;

                    elem.style.transformOrigin = transformOrigin;
                    elem.style.oTransformOrigin = transformOrigin;
                    elem.style.msTransformOrigin = transformOrigin;
                    elem.style.mozTransformOrigin = transformOrigin;
                    elem.style.webkitTransformOrigin = transformOrigin;
                },

                getCurrentMatrixTransformOrigin: function (el) {

                    var st = window.getComputedStyle(el, null);
                    var tr = st.getPropertyValue("-webkit-transform-origin") ||
                        st.getPropertyValue("-moz-transform-origin") ||
                        st.getPropertyValue("-ms-transform-origin") ||
                        st.getPropertyValue("-o-transform-origin") ||
                        st.getPropertyValue("transform-origin");

                    if (!tr || tr === 'none') {
                        this.setElTransformOriginStyle(el, '50% 50%');
                        return this.getCurrentMatrixTransformOrigin();
                    }

                    var values = tr.replace('px', '').split(' ');
                    var offset = $(el).offset();
                    var r = {
                        x: parseFloat(values[0]),
                        y: parseFloat(values[1]),
                        cssValue: tr
                    };
                    r.oCoord = {x: offset.left + r.x, y: offset.top + r.y};
                    r.rect = el.getBoundingClientRect();
                    return r;
                },

                getCurrentMatrixTransform: function (el) {

                    var st = window.getComputedStyle(el, null);
                    var tr = st.getPropertyValue("-webkit-transform") ||
                        st.getPropertyValue("-moz-transform") ||
                        st.getPropertyValue("-ms-transform") ||
                        st.getPropertyValue("-o-transform") ||
                        st.getPropertyValue("transform");

                    if (!tr || tr === 'none') {
                        this.setElTransformStyle(el, 'matrix(1,0,0,1,0,0)');
                        return {
                            a: 1,
                            b: 0,
                            c: 0,
                            d: 1,
                            e: 0,
                            f: 0,
                            cssValue: tr,
                            matrix: $M([
                                [1, 0, 0],
                                [0, 1, 0],
                                [0, 0, 1]
                            ])
                        };
                    }

                    var values = tr.split('(')[1];
                    values = values.split(')')[0];
                    values = values.split(',');
                    var r = {
                        a: parseFloat(values[0]),
                        b: parseFloat(values[1]),
                        c: parseFloat(values[2]),
                        d: parseFloat(values[3]),
                        e: parseFloat(values[4]),
                        f: parseFloat(values[5]),
                        cssValue: tr
                    };
                    r.matrix = $M([
                        [r.a, r.c, r.e],
                        [r.b, r.d, r.f],
                        [0, 0, 1]
                    ]);
                    return r;
                },

                buildCssMatrixFromM: function (m) {
                    if (!m) return false;
                    return 'matrix(' + m.e(1, 1) + ',' + m.e(2, 1) + ',' + m.e(1, 2) + ',' + m.e(2, 2) + ',' + m.e(1, 3) + ',' + m.e(2, 3) + ')';
                },

                buildCssMatrixFormMatrix: function (m) {
                    if (!m) return false;
                    return 'matrix(' + m.a + ',' + m.b + ',' + m.c + ',' + m.d + ',' + m.e + ',' + m.f + ')';
                },

                buildCssMatrixTranslate: function (tx, ty) {
                    tx = tx || 0;
                    ty = ty || 0;
                    return 'matrix(1,0,0,1,' + tx + ',' + ty + ')';
                },

                buildCssMatrixScale: function (factor) {
                    return this.buildCSSMatrixScaleXY(factor, factor);
                },

                buildCSSMatrixScaleXY: function (factorX, factorY) {
                    factorX = factorX || 1;
                    factorY = factorY || 1;
                    return 'matrix(' + factorX + ',0,0,' + factorY + ',0,0)';
                },

                buildCssMatrixTranslateScale: function (tx, ty, factor) {
                    tx = tx || 0;
                    ty = ty || 0;
                    factor = factor || 1;

                    return 'matrix(' + factor + ',0,0,' + factor + ',' + tx + ',' + ty + ')';
                },

                buildCssMatrixTranslateScaleXY: function (tx, ty, factorX, factorY) {
                    tx = tx || 0;
                    ty = ty || 0;
                    factorX = factorX || 1;
                    factorY = factorY || 1;

                    return 'matrix(' + factorX + ',0,0,' + factorY + ',' + tx + ',' + ty + ')';
                },

                buildCssMatrixRotate: function (rotate) {

                    if (!rotate) return false;

                    var rotateRad = Math.degToRad(rotate);
                    var a = Math.cos(rotateRad);
                    var b = Math.sin(rotateRad);
                    var c = -1 * Math.sin(rotateRad);
                    var d = Math.cos(rotateRad);
                    return 'matrix(' + a + ',' + b + ',' + c + ',' + d + ',0,0)';

                },

                buildCssMatrixTranslateRotate: function (tx, ty, rotate) {

                    tx = tx || 0;
                    ty = ty || 0;

                    if (!rotate) return this.buildCssMatrixTranslate(tx, ty);

                    var rotateRad = Math.degToRad(rotate);
                    var a = Math.cos(rotateRad);
                    var b = Math.sin(rotateRad);
                    var c = -1 * Math.sin(rotateRad);
                    var d = Math.cos(rotateRad);
                    return 'matrix(' + a + ',' + b + ',' + c + ',' + d + ',0,0)';

                },

                buildCssMatrix: function (tx, ty, factor, rotate) {

                    tx = tx || 0;
                    ty = ty || 0;

                    if (!rotate && !factor) return this.buildCssMatrixTranslate(tx, ty);

                    if (!rotate) return this.buildCssMatrixTranslateScale(tx, ty, factor);

                    if (!factor) return this.buildCssMatrixTranslateRotate(tx, ty, rotate);

                    var rotateRad = Math.degToRad(rotate);
                    var a = factor * Math.cos(rotateRad);
                    var b = factor * Math.sin(rotateRad);
                    var c = -1 * factor * Math.sin(rotateRad);
                    var d = factor * Math.cos(rotateRad);
                    return 'matrix(' + a + ',' + b + ',' + c + ',' + d + ',' + tx + ',' + ty + ')';

                },

                buildMatrixFromParameters: function (tx, ty, factor, rotate) {

                    tx = tx || 0;
                    ty = ty || 0;

                    rotate = rotate || 0;
                    factor = factor || 1;

                    var rotateRad = Math.degToRad(rotate);
                    var a = factor * Math.cos(rotateRad);
                    var b = factor * Math.sin(rotateRad);
                    var c = -1 * factor * Math.sin(rotateRad);
                    var d = factor * Math.cos(rotateRad);
                    return $M([
                        [a, c, tx],
                        [b, d, ty],
                        [0, 0, 1]
                    ]);

                },

                buildMatrixScaleXY: function (factorX, factorY) {
                    return $M(
                        [
                            [factorX, 0, 0],
                            [0, factorY, 0],
                            [0, 0, 1]
                        ]
                    );
                },

                decomposeMatrix: function (matrix) {

                    var deltaTransformPoint = function (matrix, point) {

                        var dx = point.x * matrix.a + point.y * matrix.c + 0;
                        var dy = point.x * matrix.b + point.y * matrix.d + 0;
                        return { x: dx, y: dy };
                    };

                    // calculate delta transform point
                    var px = deltaTransformPoint(matrix, { x: 0, y: 1 });
                    var py = deltaTransformPoint(matrix, { x: 1, y: 0 });

                    // calculate skew
                    var skewX = ((180 / Math.PI) * Math.atan2(px.y, px.x) - 90);
                    var skewY = ((180 / Math.PI) * Math.atan2(py.y, py.x));

                    return {

                        translateX: matrix.e,
                        translateY: matrix.f,
                        scaleX: Math.sqrt(matrix.a * matrix.a + matrix.b * matrix.b),
                        scaleY: Math.sqrt(matrix.c * matrix.c + matrix.d * matrix.d),
                        skewX: skewX,
                        skewY: skewY,
                        rotation: skewX // rotation is the same as skew x
                    };
                },
                zoom: function zoom($el, options) {

                    if (!$el) {
                        return;
                    }
                    var el = $el[0];

                    var defaultOptions = {isZoomIn: true, step: 0.05};
                    var opts = angular.extend(defaultOptions, options);

                    var m = this.getCurrentMatrixTransform(el);
                    var transformProperties = this.decomposeMatrix(m);
                    var currentScaleX = transformProperties.scaleX;
                    var currentScaleY = transformProperties.scaleY;

                    var scaleX = currentScaleX * (opts.isZoomIn ? 1 + opts.step : 1 - opts.step);
                    var scaleY = currentScaleY * (opts.isZoomIn ? 1 + opts.step : 1 - opts.step);

                    if (opts.minScale) {
                        if (scaleX < opts.minScale) {
                            scaleX = currentScaleX;
                        }
                        if (scaleY < opts.minScale) {
                            scaleY = currentScaleY;
                        }
                    }

                    if (opts.maxScale) {
                        if (scaleX > opts.maxScale) {
                            scaleX = currentScaleX;
                        }
                        if (scaleY > opts.maxScale) {
                            scaleY = currentScaleY;
                        }
                    }

                    var tranformStyleValue;
                    if (scaleX === scaleY)
                        tranformStyleValue = this.buildCssMatrixTranslateScale(m.e, m.f, scaleX);
                    else
                        tranformStyleValue = this.buildCssMatrixTranslateScaleXY(m.e, m.f, scaleX, scaleY);

                    this.setElTransformStyle(el, tranformStyleValue);

                    if (scaleX === scaleY)
                        return {scale: scaleX};
                    else
                        return {scaleX: scaleX, scaleY: scaleY};
                }
            };
        });

})(window, window.angular);